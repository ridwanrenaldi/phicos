<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maps extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model("M_Auth");
        $this->load->model("M_Layer");
    }

    public function index(){
        $sess = $this->M_Auth->session(array("root","admin"));
        if ($sess === FALSE) {
            redirect(site_url("admin/dashboard/logout"),"refresh");
        } else {
            $data["session"] = $sess;
            $data["sidebar"] = "maps";
            $data["layer"] = $this->M_Layer->getAll();
            $this->load->view('admin/maps/maps.php', $data);
        }
    }

    public function maps(){
        $sess = $this->M_Auth->session(array("root","admin"));
        if ($sess === FALSE) {
            redirect(site_url("admin/dashboard/logout"),"refresh");
        } else {
            $data["session"] = $sess;
            $data["sidebar"] = "maps2";
            
            $this->load->view('admin/maps/maps2.php', $data);
        }
    }
}
