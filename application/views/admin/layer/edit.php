<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("admin/partials/head.php") ?>
</head>
<body class="hold-transition sidebar-mini pace-primary">
<!-- Site wrapper -->
<div class="wrapper">
  <?php $this->load->view("admin/partials/navbar.php") ?>

  <?php $this->load->view("admin/partials/sidebar.php") ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Layer</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/layer/table') ?>">Layer</a></li>
              <li class="breadcrumb-item active">Ubah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Input</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?php echo site_url('admin/layer/edit/'.$layer_id) ?>" class="form-horizontal" enctype="multipart/form-data" method="post">
                <div class="card-body">
                <div class="form-group row">
                    <label for="_nama_" class="col-sm-3 col-form-label" style="text-align: right;">Nama</label>
                    <div class="col-sm-6">
                      <input type="text" name="_nama_" class="form-control" id="_nama_" placeholder="Nama" value="<?php if (set_value('_nama_') != null) { echo set_value('_nama_'); } else { echo $layer['layer_nama']; } ?>" required="required" minlength="2" maxlength="50">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="_warna_" class="col-sm-3 col-form-label" style="text-align: right;">Warna</label>
                    <div class="col-sm-6">
                        <div class="input-group my-colorpicker2">
                            <input type="text" name="_warna_" class="form-control" id="_warna_" value="<?php if (set_value('_warna_') != null) { echo set_value('_warna_'); } else { echo $layer['layer_warna']; } ?>" required="required">

                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-square"></i></span>
                            </div>
                        </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="_file_" class="col-sm-3 col-form-label" style="text-align: right;">File</label>
                    <div class="col-sm-6">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="_file_" name="_file_" accept="application/octet-stream">
                            <input type="hidden" id="_checkfile_" name="_checkfile_" value="_file_">
                            <label class="custom-file-label" for="customFile" id="filename" style="overflow: hidden;"><?php if (!empty($layer['layer_file'])) { echo $layer['layer_file']; } else { echo "Choose file"; } ?></label>
                        </div>
                    </div>
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <div class="form-group row">
                    <div class="col-md-6 col-sm-6 offset-md-3">
                      <button type="button" class="btn btn-warning" onclick="goBack()">Batal</button>
                      <button type="reset" class="btn btn-info" id="_reset_">Reset</button>
                      <button type="submit" class="btn btn-success">Kirim</button>
                    </div>
                  </div>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view("admin/partials/footer.php") ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php $this->load->view("admin/partials/javascript.php") ?>
<script>
  function goBack() {
    window.history.back();
  }

  $(document).ready(function(){
    $('.my-colorpicker2').colorpicker();
    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("#_file_").on("change", function() {
        var files = $(this).get(0).files;
        var eks = (files[0].name).split('.')[1]
        if (eks.toLowerCase() != "geojson") {
            $("#filename").text("");
            $("#_file_").val("");
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                html: 'The filetype you are attempting to upload is not allowed'
            })
        } else {
            $("#filename").text(files[0].name);
        }
    });

    $("#_reset_").on("click", function(){
        $("#filename").text("Choose file");
    });

    // ===== Notification alert =====
    var notif = {
      status:"<?php if(isset($notif["status"])) { echo $notif["status"]; } ?>", 
      message:"<?php if(isset($notif["message"])) { echo $notif["message"]; } ?>"
    };

    if (notif.status == "error" && notif.message != "") {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        html: notif.message
      });
    } else if(notif.status == "success" && notif.message != ""){
      Swal.fire({
        icon: 'success',
        title: 'Success...',
        html: notif.message,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Check Data',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        if (result.value) {
          window.location.replace("<?php echo site_url('admin/layer/table') ?>");
        }
      });
    }

  });
</script>
</body>
</html>
