    
    <link href="<?php echo base_url('assets/leaflet/LeafletJS/leaflet.css')?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/leaflet/LeafletJS/leaflet.js')?>"></script>
    <link href="<?php echo base_url('assets/leaflet/LeafletFullscreen/Control.FullScreen.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/leaflet/LeafletZoomHome/dist/leaflet.zoomhome.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/leaflet/LeafletLocateControl/dist/L.Control.Locate.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/leaflet/LeafletPolylineMeasure/Leaflet.PolylineMeasure.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/leaflet/LeafletSearch/src/leaflet-search.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/leaflet/LeafletStyledLayerControl/css/styledLayerControl.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/leaflet/LeafletModal/dist/leaflet.modal.css')?>" rel="stylesheet">
    <style>
        #mapid { height: 480px; }
        .map {
            position: relative;
            width: 100%;
            height: 450px;
            overflow: hidden;
        }

        .legend {
            line-height: 18px;
            color: #555;
        }

        .info.legend {
            background-color: #fff;
            padding: 5px;
        }

        .legend i {
            width: 18px;
            height: 18px;
            float: left;
            margin-right: 5px;
            opacity: 0.8;
        }

        .leaflet-top.leaflet-right .leaflet-control-layers:nth-child(1) .leaflet-control-layers-toggle {
            background-image: url("<?php echo base_url('assets/images/basemap.png') ?>");
        }

        .leaflet-top.leaflet-right .leaflet-control-layers:nth-child(2) .leaflet-control-layers-toggle {
            background-image: url("<?php echo base_url('assets/images/overlayer.png') ?>");
        }
    </style>