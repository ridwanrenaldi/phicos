    
    <script src="<?php echo base_url('assets/leaflet/LeafletAjax/dist/leaflet.ajax.js')?>"></script>
    <script src="<?php echo base_url('assets/leaflet/LeafletFullscreen/Control.FullScreen.js')?>"></script>
    <script src="<?php echo base_url('assets/leaflet/LeafletZoomHome/dist/leaflet.zoomhome.js')?>"></script>
    <script src="<?php echo base_url('assets/leaflet/LeafletLocateControl/dist/L.Control.Locate.min.js')?>"></script>
    <script src="<?php echo base_url('assets/leaflet/LeafletPolylineMeasure/Leaflet.PolylineMeasure.js')?>"></script>
    <script src="<?php echo base_url('assets/leaflet/LeafletSearch/src/leaflet-search.js')?>"></script>
    <script src="<?php echo base_url('assets/leaflet/LeafletStyledLayerControl/src/styledLayerControl.js')?>"></script>
    <script src="<?php echo base_url('assets/leaflet/LeafletModal/dist/L.Modal.js')?>"></script>
    <script src="<?php echo base_url('assets/leaflet/LeafletEasyPrint2/dist/bundle.js')?>"></script>