<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("admin/partials/head.php") ?>
  <?php $this->load->view("admin/partials/leafletcss") ?>
</head>
<body class="hold-transition sidebar-mini pace-primary">
<!-- Site wrapper -->
<div class="wrapper">
  <?php $this->load->view("admin/partials/navbar.php") ?>

  <?php $this->load->view("admin/partials/sidebar.php") ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Maps</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/maps/maps') ?>">Maps</a></li>
              <li class="breadcrumb-item active">Maps</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Kota Surakarta</h3>
              </div>
              <!-- /.card-header -->

                <div class="card-body">
                    <div class="row">
                      <div id="mapid" class="map"></div>
                    </div>
                </div>
                <!-- /.card-body -->


            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view("admin/partials/footer.php") ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php $this->load->view("admin/partials/javascript.php") ?>
<?php $this->load->view("admin/partials/leafletjs") ?>
<?php $this->load->view("admin/partials/providers") ?>
    <script>
        var maps = L.map('mapid', {
            fullscreenControl: true,
            fullscreenControlOptions: {position: 'topleft'},
            zoomControl: false,
        }).setView([-7.556202,110.823247], 13);

        var zoomHome = L.Control.zoomHome().addTo(maps);

        L.control.polylineMeasure ({
            position:'topleft', 
            unit:'metres', 
            showBearings:true, 
            clearMeasurementsOnStop: false, 
            showClearControl: true, 
            showUnitControl: true
        }).addTo (maps);

        var locate = L.control.locate().addTo(maps);


        function popupLayer(e) {
            var popup = L.popup();
            var html = "\
              <table>\
                <tr>\
                  <td>Kecamatan</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.KECAMATAN+"</td>\
                </tr>\
                <tr>\
                  <td>Kelurahan</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.KELURAHAN+"</td>\
                </tr>\
                <tr>\
                  <td>RT</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.RT+"</td>\
                </tr>\
                <tr>\
                  <td>RW</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.RW+"</td>\
                </tr>\
                <tr>\
                  <td>Luas</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.LUASHA+"</td>\
                </tr>\
              </table>\
            ";
            popup
            .setLatLng(e.latlng)
            .setContent(html)
            .openOn(maps);
            // return popup;
        }

        function modalinfo(e){
          maps.fire('modal', {
            content: "\
            <h5 class='text-center'>Detail Kawasan</h5>\
            <table class='table'>\
                <tr>\
                  <td>Kecamatan</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.KECAMATAN+"</td>\
                </tr>\
                <tr>\
                  <td>Kelurahan</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.KELURAHAN+"</td>\
                </tr>\
                <tr>\
                  <td>RT</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.RT+"</td>\
                </tr>\
                <tr>\
                  <td>RW</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.RW+"</td>\
                </tr>\
                <tr>\
                  <td>Luas</td>\
                  <td>:</td>\
                  <td>"+e.sourceTarget.feature.properties.LUASHA+"</td>\
                </tr>\
              </table>"
          });
        }

        function setstyle(warna){
          return style = {
              fillColor: warna, 
              fillOpacity: 0.15,
              color: warna,
              weight: 1.5,
          };
        }

        function highlightFeature(e) {
            let highlight = { fillOpacity: 0.4, weight: 3}
            e.target.setStyle(highlight);
            // popupLayer(e);
        }

        function resetHighlight(e) {
            let style = { fillOpacity: 0.15, weight: 1.5}
            let LayerName = e.target.options.LayerName
            e.target.setStyle(style);
        }

        function onEachFeature(feature, layer) {
            layer.on({
              mouseover: highlightFeature,
              mouseout: resetHighlight,
              click: modalinfo
            })
        }


        var basemaps = [
          { 
            groupName : "Google Base Maps",
            expanded : true,
            layers    : {
              "Google Streets": providers["googleStreets"].addTo(maps),
              "Google Hybrid": providers["googleHybrid"],
              "Google Satellite": providers["googleSat"],
              "Google Terrain": providers["googleTerrain"],
            }
          }, 
          
          {
            groupName : "OSM Base Maps",
            expanded : true,
            layers    : {
              "OSM": providers["OSM"],
              "OSM BW": providers["OSM BW"],
              "OSM DE": providers["OSM DE"],
              "TONER": providers["TONER"],
              "POSITRON": providers["POSITRON"],
            }
          }, 
          					
        ];	

        var overlayer = [
          {
            groupName : "Rencana Tata Ruang",
            expanded : true,
            layers    : { 
              "Kawasan Kumuh" : L.geoJson.ajax("<?= site_url('admin/API/geojson/getkawasankumuh') ?>", {onEachFeature : onEachFeature, style:setstyle("#FF2631")}),
              "Kawasan Ilegal" 	 : L.geoJson.ajax("<?= site_url('admin/API/geojson/getkawasanilegal') ?>", {onEachFeature : onEachFeature, style:setstyle("#2D00ED")}),
              "Kawasan Budidaya" : L.geoJson.ajax("<?= site_url('admin/API/geojson/getkawasanbudaya') ?>", {onEachFeature : onEachFeature, style:setstyle("#16E716")}),
            }	
          } 
			  ];

        var options = {
          container_width 	: "300px",
          group_maxHeight     : "80px",
          //container_maxHeight : "350px", 
          exclusive       	: false,
          collapsed : true, 
          position: 'topright'
        };


        var base = L.Control.styledLayerControl(basemaps, null, options).addTo(maps);
        var over = L.Control.styledLayerControl(null, overlayer).addTo(maps);


        L.easyPrint({
          title: 'Print Button',
          position: 'bottomleft',
          exportOnly: true,
          sizeModes: ['A4Portrait', 'A4Landscape']
        }).addTo(maps);



    </script>
</body>
</html>
