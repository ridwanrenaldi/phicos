<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("admin/partials/head.php") ?>
  <?php $this->load->view("admin/partials/leafletcss") ?>
</head>
<body class="hold-transition sidebar-mini pace-primary">
<!-- Site wrapper -->
<div class="wrapper">
  <?php $this->load->view("admin/partials/navbar.php") ?>

  <?php $this->load->view("admin/partials/sidebar.php") ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Maps</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/maps/index') ?>">Maps</a></li>
              <li class="breadcrumb-item active">Maps</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Kota Surakarta</h3>
              </div>
              <!-- /.card-header -->

                <div class="card-body">
                    <div class="row">
                      <div id="mapid" class="map"></div>
                    </div>
                </div>
                <!-- /.card-body -->


            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view("admin/partials/footer.php") ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php $this->load->view("admin/partials/javascript.php") ?>
<?php $this->load->view("admin/partials/leafletjs") ?>
<?php $this->load->view("admin/partials/providers") ?>
    <script>
        var maps = L.map('mapid', {
            fullscreenControl: true,
            fullscreenControlOptions: {position: 'topleft'},
            zoomControl: false,
        }).setView([-7.556202,110.823247], 13);

        var zoomHome = L.Control.zoomHome().addTo(maps);

        L.control.polylineMeasure ({
            position:'topleft', 
            unit:'metres', 
            showBearings:true, 
            clearMeasurementsOnStop: false, 
            showClearControl: true, 
            showUnitControl: true
        }).addTo (maps);

        var locate = L.control.locate().addTo(maps);

        var basemaps = {
            "Google Streets": providers["googleStreets"].addTo(maps),
            "Google Hybrid": providers["googleHybrid"],
            "Google Satellite": providers["googleSat"],
            "Google Terrain": providers["googleTerrain"],
            "OSM": providers["OSM"],
            "OSM BW": providers["OSM BW"],
            "OSM DE": providers["OSM DE"],
            "TONER": providers["TONER"],
            "POSITRON": providers["POSITRON"],
        };
        var overlayer = {};
      
    
        var style = {};

        function popupLayer(e) {
          console.log(e);
            var popup = L.popup();

            var html = "";
            if (e.sourceTarget.feature.properties.KAB_1) {
              html = "\
                        <table>\
                          <tr>\
                            <td>Kecamatan</td>\
                            <td>:</td>\
                            <td>"+e.sourceTarget.feature.properties.KECAMATAN+"</td>\
                          </tr>\
                          <tr>\
                            <td>Kelurahan</td>\
                            <td>:</td>\
                            <td>"+e.sourceTarget.feature.properties.KELURAHAN+"</td>\
                          </tr>\
                          <tr>\
                            <td>RT</td>\
                            <td>:</td>\
                            <td>"+e.sourceTarget.feature.properties.RT+"</td>\
                          </tr>\
                          <tr>\
                            <td>RW</td>\
                            <td>:</td>\
                            <td>"+e.sourceTarget.feature.properties.RW+"</td>\
                          </tr>\
                          <tr>\
                            <td>Luas</td>\
                            <td>:</td>\
                            <td>"+e.sourceTarget.feature.properties.Luas_Ha+"</td>\
                          </tr>\
                        </table>\
                      ";
            } else {
              html = e.sourceTarget.feature.properties.NAMA;
            }

            popup
            .setLatLng(e.latlng)
            .setContent(html)
            .openOn(maps);
            // return popup;
        }

        function highlightFeature(e) {
            let highlight = { fillOpacity: 0.4, weight: 3}
            e.target.setStyle(highlight);
            popupLayer(e);
        };
        function resetHighlight(e) {
            let LayerName = e.target.options.LayerName
            e.target.setStyle(style[LayerName]);
        };
        function onEachFeature(feature, layer) {
            layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            })
        };

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/API/layer/data')?>",
            dataType: "JSON",
            success: function (data) {
              if (data) {
                  let baseURL = "<?php echo base_url('uploads/layer/') ?>";
                  for (let i = 0; i < data.length; i++) {

                  style[data[i].layer_nama] = {
                      fillColor: data[i].layer_warna, 
                      fillOpacity: 0.15,
                      color: data[i].layer_warna,
                      weight: 1.5, 
                      LayerName: data[i].layer_nama
                  };

                  overlayer[data[i].layer_nama] = L.geoJson.ajax(baseURL+data[i].layer_file, {onEachFeature : onEachFeature, style:style[data[i].layer_nama]});
                  }
                  // console.log(data);
              } else {
                  console.log("Data Tidak Ada");
              }
            }
        });


        $( document ).ajaxComplete(function() {
            // console.log(overlayer);
            L.control.layers(basemaps, overlayer).addTo(maps);
        });

    </script>
</body>
</html>
