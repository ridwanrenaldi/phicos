<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Geojson extends CI_Model {
    
    public function __construct(){
        parent::__construct();
    }

    public function getKawasanKumuh($where=null){
      $this->db->select("OGR_FID, objectid, kecamatan, rw, rt, 
                          kelurahan, kab_1, sk_baru, kawasan, luas_ha, 
                          long, lat, rtlh, jln_rsk, air_mnm, dra_rsk, 
                          ipal, sampah, prt_kbkrn, ST_asGeoJson(SHAPE) as geom");
      $this->db->from("kawasan_kumuh");
      
      if ($where != null) {
        $this->db->where($where);
      }

      $data = $this->db->get()->result_array();
      $json = array();
      foreach ($data as $key => $value) {
          $properties = array(
              "OGR_FID"   => $value["OGR_FID"],
              "OBJECTID"  => $value["objectid"],
              "KECAMATAN" => $value["kecamatan"],
              "RW"        => $value["rw"],
              "RT"        => $value["rt"],
              "KELURAHAN" => $value["kelurahan"],
              "KAB1"      => $value["kab_1"],
              "SKBARU"    => $value["sk_baru"],
              "KAWASAN"   => $value["kawasan"],
              "LUASHA"    => $value["luas_ha"],
              "LONG"      => $value["long"],
              "LAT"       => $value["lat"],
              "RTLH"      => $value["rtlh"],
              "JLNRSK"    => $value["jln_rsk"],
              "AIRMNM"    => $value["air_mnm"],
              "DRARSK"    => $value["dra_rsk"],
              "IPAL"      => $value["ipal"],
              "SAMPAH"    => $value["sampah"],
              "PRTKBKRN"  => $value["prt_kbkrn"],
          );
          $geometry = json_decode($value["geom"], true);
          $features = array(
              "type" => "Feature",
              "properties" => $properties,
              "geometry" => $geometry
          );
  
          $json[] = $features;
      }

      $geoJSON = array(
          "type" => "FeatureCollection",
          "features" => $json
      );
  
      return json_encode($geoJSON);
    }

    public function getKawasanIlegal($where=null){
      $this->db->select("OGR_FID, objectid_1, kab_1, 
                        kecamatan, rw, rt, kelurahan, 
                        luas_ha, status, ST_asGeoJson(SHAPE) as geom");
      $this->db->from("kawasan_ilegal");

      if ($where != null) {
        $this->db->where($where);
      }

      $data = $this->db->get()->result_array();
      $json = array();
      foreach ($data as $key => $value) {
          $properties = array(
              "OGR_FID"   => $value["OGR_FID"],
              "OBJECTID"  => $value["objectid_1"],
              "KECAMATAN" => $value["kecamatan"],
              "RW"        => $value["rw"],
              "RT"        => $value["rt"],
              "KELURAHAN" => $value["kelurahan"],
              "KAB1"      => $value["kab_1"],
              "LUASHA"    => $value["luas_ha"],
              "STATUS"    => $value["status"],
          );
          $geometry = json_decode($value["geom"], true);
          $features = array(
              "type" => "Feature",
              "properties" => $properties,
              "geometry" => $geometry
          );
  
          $json[] = $features;
      }

      $geoJSON = array(
          "type" => "FeatureCollection",
          "features" => $json
      );
  
      return json_encode($geoJSON);
    }

    public function getKawasanBudaya($where=null){
      $this->db->select("OGR_FID, objectid_1, kab_1, 
                        kecamatan, rw, rt, kelurahan, 
                        luas_ha, status, ST_asGeoJson(SHAPE) as geom");
      $this->db->from("kawasan_cagar_budaya");

      if ($where != null) {
        $this->db->where($where);
      }

      $data = $this->db->get()->result_array();
      $json = array();
      foreach ($data as $key => $value) {
          $properties = array(
              "OGR_FID"   => $value["OGR_FID"],
              "OBJECTID"  => $value["objectid_1"],
              "KECAMATAN" => $value["kecamatan"],
              "RW"        => $value["rw"],
              "RT"        => $value["rt"],
              "KELURAHAN" => $value["kelurahan"],
              "KAB1"      => $value["kab_1"],
              "LUASHA"    => $value["luas_ha"],
              "STATUS"    => $value["status"],
          );
          $geometry = json_decode($value["geom"], true);
          $features = array(
              "type" => "Feature",
              "properties" => $properties,
              "geometry" => $geometry
          );
  
          $json[] = $features;
      }

      $geoJSON = array(
          "type" => "FeatureCollection",
          "features" => $json
      );
  
      return json_encode($geoJSON);
    }

}
?>