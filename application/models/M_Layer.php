<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Layer extends CI_Model {
    private $table = "layer"; // table in database
    
    public function __construct(){
        parent::__construct();
    }

    public function rules($mode){
        $rules = array(
            array(  'field' => '_nama_',
                    'label' => 'Name',
                    'rules' => 'required|trim|alpha_numeric_spaces|min_length[3]|max_length[20]'),
      
            array(  'field' => '_warna_',
                    'label' => 'Color',
                    'rules' => 'required|trim|min_length[6]|max_length[8]'),

        );

        if ($mode == "add") {
            $rules[] = array(  'field' => '_checkfile_',
                                'label' => 'File',
                                'rules' => 'callback_checkFileGeojson[add]');
        } else {
            $rules[] = array(  'field' => '_checkfile_',
                                'label' => 'File',
                                'rules' => 'callback_checkFileGeojson[edit]');
        }

        return $rules;
    }

    public function uploadFile($oldpath=null, $name="_file_"){
        if( file_exists($_FILES[$name]['tmp_name']) ){
            $config['upload_path']          = './uploads/layer/';
            $config['allowed_types']        = '*';
            $config['max_size']             = 204800; // 200MB
            $config['max_width']            = 0; // pixel
            $config['max_height']           = 0; // pixel
            $config['overwrite']            = TRUE;
            $config['encrypt_name']         = TRUE;
            $config['remove_spaces']		= TRUE;
        
            $this->load->library("upload", $config);
            if ( ! $this->upload->do_upload($name)){
                $response = array(
                    "status" => "error",
                    "message" => $this->upload->display_errors()
                );
            }else{
                if ($oldpath != null) {
                    if (file_exists($oldpath)) {
                        unlink($oldpath);
                    }
                }

                $response = array(
                    "status" => "success",
                    "message" => "Image uploaded successfully.",
                    "data" => $this->upload->data()
                );
            }

        } else {
            $response = array(
            "status" => "empty",
            "message" => "Choose file to upload."
            );
        }

        return $response;
    }

    public function getById($id){
        return $this->db->get_where($this->table, array("layer_id" => $id) )->row_array();
    }

    public function getAll($fields=null, $where=null) {
        if($fields != null){
            $this->db->select($fields);
        }
        if($where != null){
            $this->db->where($where);
        }
        return $this->db->get($this->table)->result_array();
    }

    public function insert(){
        $post = $this->input->post();
        if (!empty($post)){
            $uploadFile = $this->uploadFile();
            if ($uploadFile["status"] == "success") {
                $data = array(
                    "layer_id"         => NULL,
                    "layer_nama"       => htmlspecialchars($post["_nama_"]),
                    "layer_warna"      => htmlspecialchars($post["_warna_"]),
                    "layer_file"       => $uploadFile["data"]["file_name"],
                );
    
                $data = $this->security->xss_clean($data);
                if($this->db->insert($this->table, $data)){
                    $response = array(
                    "status" => "success",
                    "message" => "Success insert data",
                    );
                } else {
                    $response = array(
                    "status" => "error",
                    "message" => "Failed insert data",
                    );
                }
            } else {
                $response = $uploadFile;
            }
        } else {
            $response = array(
                "status" => "error",
                "message" => "Data not found!",
            );
        }
        return $response;
    }

    public function update($id){
        $post = $this->input->post();
        if (!empty($post)){
            $layer = $this->getById($id);
            if (!empty($layer["layer_file"])) {
                $oldpath = './uploads/layer/'.$layer["layer_file"];
                $uploadFile = $this->uploadFile($oldpath);
                if ($uploadFile["status"] != "error") {
                    $data = array(
                        "layer_nama"       => htmlspecialchars($post["_nama_"]),
                        "layer_warna"      => htmlspecialchars($post["_warna_"]),
                    );

                    if ($uploadFile["status"] == "success") {
                        $data["layer_file"] = $uploadFile["data"]["file_name"];
                    }
        
                    $data = $this->security->xss_clean($data);
                    $this->db->where("layer_id", $id);
                    if($this->db->update($this->table, $data)){
                        $response = array(
                            "status" => "success",
                            "message" => "Success update data",
                        );
                    } else {
                        $response = array(
                            "status" => "error",
                            "message" => "Failed update data",
                        );
                    }
                } else {
                    $response = $uploadFile;
                }
            } else {
                $response = array(
                    "status" => "error",
                    "message" => "Data not found!",
                );
            }
        } else {
            $response = array(
                "status" => "error",
                "message" => "Data not found!",
            );
        }
        return $response;
    }

    function delete($id) {
        $this->db->where('layer_id', $id);
        if($this->db->delete($this->table)){
            $response = array(
                "status" => "success",
                "message" => "Data deleted successfully",
            );
        } else {
            $response = array(
                "status" => "error",
                "message" => "Data failed to delete",
            );
        }
        return $response;
    }

}
?>